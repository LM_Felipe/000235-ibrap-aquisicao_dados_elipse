;============================================================
; Profibus Device Database of :
; Schneider Electric Gateways
; Model       : LUFP7 
; Description : Profibus-DP/Modbus Gateway
; Language    : English
; Date        : 14 November 2002
; Author      : Schneider Electric [EB/JFR]
;============================================================
#Profibus_DP

GSD_Revision        = 2

; Device identification
Vendor_Name         = "Telemecanique"
Model_Name          = "LUFP7"
Revision            = "Version 1.0"
Ident_Number        = 0x071F
Protocol_Ident      = 0              ; DP protocol
Station_Type        = 0              ; Slave device
FMS_supp            = 0              ; FMS not supported
Hardware_Release    = "Version 1.41"
Software_Release    = "Version 1.12"

; Supported baudrates
9.6_supp            = 1
19.2_supp           = 1
45.45_supp          = 1
93.75_supp          = 1
187.5_supp          = 1
500_supp            = 1
1.5M_supp           = 1
3M_supp             = 1
6M_supp             = 1
12M_supp            = 1

; Maximum responder time for supported baudrates 
MaxTsdr_9.6         = 60
MaxTsdr_19.2        = 60
MaxTsdr_45.45       = 60
MaxTsdr_93.75       = 60
MaxTsdr_187.5       = 60
MaxTsdr_500         = 100
MaxTsdr_1.5M        = 150
MaxTsdr_3M          = 250
MaxTsdr_6M          = 450
MaxTsdr_12M         = 800

; Supported hardware features
Redundancy          = 0       ; not supported
Repeater_Ctrl_Sig   = 2       ; TTL
24V_Pins            = 0       ; not connected
Implementation_Type = "SPC3"

; Supported DP features
Freeze_Mode_supp    = 1       ; supported
Sync_Mode_supp      = 1       ; supported
Auto_Baud_supp      = 1       ; supported
Set_Slave_Add_supp  = 0       ; not supported

; Maximum polling frequency
Min_Slave_Intervall = 1       ; 100 us

; Maximum supported sizes
Modular_Station     = 1       ; modular
Max_Module          = 24
Max_Input_Len       = 244
Max_Output_Len      = 244
Max_Data_Len        = 416
Modul_Offset        = 1

Fail_Safe           = 0       ; state CLEAR not accepted

Slave_Family        = 0
Max_Diag_Data_Len   = 6

Bitmap_Device = "LUFP7_R"
Bitmap_Diag   = "LUFP7_D"
Bitmap_SF     = "LUFP7_S"

; Definition of modules
Module = "IN/OUT:   1 Byte" 0x30
EndModule
;
Module = "IN/OUT:   2 Byte ( 1 word)" 0x70
EndModule
;
Module = "IN/OUT:   4 Byte ( 2 word)" 0x71
EndModule
;
Module = "IN/OUT:   6 Byte ( 3 word)" 0x72
EndModule
;
Module = "IN/OUT:   8 Byte ( 4 word)" 0x73
EndModule
;
Module = "IN/OUT:  10 Byte ( 5 word)" 0x74
EndModule
;
Module = "IN/OUT:  12 Byte ( 6 word)" 0x75
EndModule
;
Module = "IN/OUT:  14 Byte ( 7 word)" 0x76
EndModule
;
Module = "IN/OUT:  16 Byte ( 8 word)" 0x77
EndModule
;
Module = "IN/OUT:  32 Byte (16 word)" 0x7F
EndModule
;
Module = "IN/OUT:  64 Byte (32 word)" 0xC0,0x5F,0x5F
EndModule
;
Module = "IN/OUT: 128 Byte (64 word)" 0xC0,0x7F,0x7F
EndModule
;
Module = "INPUT:    1 Byte" 0x10
EndModule
;
Module = "INPUT:    2 Byte ( 1 word)" 0x50
EndModule
;
Module = "INPUT:    4 Byte ( 2 word)" 0x51
EndModule
;
Module = "INPUT:    6 Byte ( 3 word)" 0x52
EndModule
;
Module = "INPUT:    8 Byte ( 4 word)" 0x53
EndModule
;
Module = "INPUT:   10 Byte ( 5 word)" 0x54
EndModule
;
Module = "INPUT:   12 Byte ( 6 word)" 0x55
EndModule
;
Module = "INPUT:   14 Byte ( 7 word)" 0x56
EndModule
;
Module = "INPUT:   16 Byte ( 8 word)" 0x57
EndModule
;
Module = "INPUT:   32 Byte (16 word)" 0x5F
EndModule
;
Module = "INPUT:   64 Byte (32 word)" 0x40,0x5F
EndModule
;
Module = "INPUT:  128 Byte (64 word)" 0x40,0x7F
EndModule
;
Module = "OUTPUT:   1 Byte" 0x20
EndModule
;
Module = "OUTPUT:   2 Byte ( 1 word)" 0x60
EndModule
;
Module = "OUTPUT:   4 Byte ( 2 word)" 0x61
EndModule
;
Module = "OUTPUT:   6 Byte ( 3 word)" 0x62
EndModule
;
Module = "OUTPUT:   8 Byte ( 4 word)" 0x63
EndModule
;
Module = "OUTPUT:  10 Byte ( 5 word)" 0x64
EndModule
;
Module = "OUTPUT:  12 Byte ( 6 word)" 0x65
EndModule
;
Module = "OUTPUT:  14 Byte ( 7 word)" 0x64
EndModule
;
Module = "OUTPUT:  16 Byte ( 8 word)" 0x67
EndModule
;
Module = "OUTPUT:  32 Byte (16 word)" 0x6F
EndModule
;
Module = "OUTPUT:  64 Byte (32 word)" 0x80,0x5F
EndModule
;
Module = "OUTPUT: 128 Byte (64 word)" 0x80,0x7F
EndModule
